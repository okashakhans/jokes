//
//  Joke.swift
//  Joke
//
//  Created by Okasha Khan on 15/06/2021.
//

import Foundation


struct Joke: Codable {
    
    let id: Int
    let type: String
    let setup: String
    let punchline: String
    
}
