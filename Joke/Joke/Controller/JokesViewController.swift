//
//  JokesViewController.swift
//  Joke
//
//  Created by Okasha Khan on 15/06/2021.
//

import UIKit
import Foundation

class JokesViewController: UIViewController {
    
    
    
   private enum Section {
        
        case general(model:[Joke])
        case knockknock(model:[Joke])
        case programming(model:[Joke])
        
    }

    // MARK: - Setups
    
    private var collectionView: UICollectionView = UICollectionView(
        frame: .zero,
        collectionViewLayout: UICollectionViewCompositionalLayout { sectionIndex, _ -> NSCollectionLayoutSection? in
            return createSectionLayout(section: sectionIndex)
        }
    )
    
    private var displayData: [Section] = []
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         print("JokesViewController")
        
        fetchEvents()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.frame = view.bounds
    }
    
    
    // MARK: - Functions

    /// The function will call API Manager to fetch jokes.
    /// It will send a dispatch group to find three different categories
    /// If successfull append data in data model.
    /// It will configure collection view and reload data.
    private func fetchEvents(){
        
        self.displayData.removeAll()
        
        let group = DispatchGroup()
        group.enter()
        group.enter()
        group.enter()
        
        APIManager.shared.fetchEvents(category: "general") { result in
            
            defer {
                group.leave()
            }
            switch result {
            
            case .success(let results):
 
                
                self.displayData.append(.general(model: results.compactMap({ return Joke(id: $0.id, type: $0.type, setup: $0.setup, punchline: $0.punchline)})))
                
            case .failure(let error):  print("Error \(error)")
            }
        }
        
        APIManager.shared.fetchEvents(category: "knock-knock") { result in
            
            defer {
                group.leave()
            }
            switch result {
            
            case .success(let results):
 
                
                self.displayData.append(.knockknock(model: results.compactMap({ return Joke(id: $0.id, type: $0.type, setup: $0.setup, punchline: $0.punchline)})))
                
            case .failure(let error):  print("Error \(error)")
            }
        }
        
        APIManager.shared.fetchEvents(category: "programming") { result in
            
            defer {
                group.leave()
            }
            switch result {
            
            case .success(let results):
 
                
                self.displayData.append(.programming(model: results.compactMap({ return Joke(id: $0.id, type: $0.type, setup: $0.setup, punchline: $0.punchline)})))
                
            case .failure(let error):  print("Error \(error)")
            }
        }
        
        group.notify(queue: .main) {
           
            self.configureCollectionView()
        }
    }
    
    
    // MARK: - Collection views Configuration
    
    private func configureCollectionView() {
        view.addSubview(collectionView)
        
        collectionView.register(UICollectionViewCell.self,forCellWithReuseIdentifier: "cell")
        self.collectionView.register(UINib.init(nibName: "JokeCategoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "JokeCategoryCollectionViewCell")
        
        collectionView.allowsSelection = true
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = .systemBackground
    }
    
    static func createSectionLayout(section: Int) -> NSCollectionLayoutSection {
    
        let item = NSCollectionLayoutItem(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1.0),
                heightDimension: .fractionalHeight(1.0)
            )
        )

        item.contentInsets = NSDirectionalEdgeInsets(top: 2, leading: 2, bottom: 2, trailing: 2)

        let group = NSCollectionLayoutGroup.vertical(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1),
                heightDimension:.fractionalWidth(0.29)
            ),
            subitem: item,
            count: 1
        )

        let section = NSCollectionLayoutSection(group: group)
        return section
    }
}


    // MARK: - Extensions (Collection View)

extension JokesViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return displayData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let result = displayData[indexPath.row]
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: JokeCategoryCollectionViewCell.identifier,for: indexPath) as? JokeCategoryCollectionViewCell else {return UICollectionViewCell()}
        
        switch result {
        
        case.general(model: let jokes):
            
            cell.configure(with: "globe", title: "General", totalJokes: jokes.count, tintColor: UIColor.yellow)
            
            return cell
            
        case.knockknock(model: let jokes):
        
            cell.configure(with: "hand.point.up.braille.fill", title: "knock-knock", totalJokes: jokes.count, tintColor: UIColor.red)
            
            return cell
        
        case.programming(model: let jokes):
        
            cell.configure(with: "macpro.gen3.server", title: "Programming", totalJokes: jokes.count, tintColor: UIColor.green)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        let result = displayData[indexPath.row]
        
        switch result {
        
            case .general(model: let jokes):
                
                self.displayJokeDetailViewController(with: jokes, category: "General")
                
            case.knockknock(model: let jokes):
                
                self.displayJokeDetailViewController(with: jokes, category: "Knock-Knock")
                
            case.programming(model: let jokes):
                
                self.displayJokeDetailViewController(with: jokes, category: "Programming")
        }
    }
    
    private func displayJokeDetailViewController(with model:[Joke], category:String) {
        
        let vc = JokeDetailViewController(jokes: model, jokeCategory: category)
        vc.navigationItem.largeTitleDisplayMode = .never
        vc.navigationItem.largeTitleDisplayMode = .never
        navigationController?.pushViewController(vc, animated: true)
        
    }
}
