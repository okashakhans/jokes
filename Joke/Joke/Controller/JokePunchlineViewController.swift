//
//  JokePunchlineViewController.swift
//  Joke
//
//  Created by Okasha Khan on 15/06/2021.
//

import UIKit

class JokePunchLineViewController: UIViewController {

    
    // MARK: - Setups
    
    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .insetGrouped)

        tableView.register(UITableViewCell.self,forCellReuseIdentifier: "cell")
        return tableView
    }()
    
    private let jokes: Joke
    private var nameOfCategory:String = ""
    
    
    // MARK: - Lifecylce
    init(jokes: Joke, jokeCategory title:String) {
        
        self.jokes = jokes
        self.nameOfCategory = title
        
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .systemBackground
        
        view.addSubview(tableView)
        self.title = nameOfCategory
        self.updateUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableView.frame = view.bounds
    }
    
    // MARK: - Functions
    
    private func updateUI() {
        
        tableView.isHidden = false
        tableView.delegate = self
        tableView.dataSource = self
    }
    // MARK: - Configurations



}

// MARK: - Extensions (Table View)
extension JokePunchLineViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell",for: indexPath) as UITableViewCell
        
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = jokes.punchline
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
