//
//  APIManager.swift
//  Joke
//
//  Created by Okasha Khan on 15/06/2021.
//

import Foundation

final class APIManager {
    
    // MARK: - Setups
    
    static let shared = APIManager()
    
    private init() {}
    
    struct Constants {
        
        static let baseAPIURL = "https://official-joke-api.appspot.com/jokes"
    }

    enum APIError: Error {
        
        case failedToGetData
        case failedToDownloadReadMeFile
    }

    enum HTTPMethod: String {
        
        case GET
        case PUT
        case POST
        case DELETE
    }


    
    // MARK: - API CALLERS
    
    /// The function will create url request and return Jokes
    /// - Note: It format Json data into readable format
    /// - Note: It return success on main thread.
    /// - Parameters:
    ///   - completion: completion handler for response
    public func fetchEvents(category:String,completion: @escaping (Result<[Joke], Error>) -> Void) {
        
        createRequest(
            with:  URL(string: Constants.baseAPIURL + "/\(category)/ten"),
            type: .GET
        ) { request in
            
            let task = URLSession.shared.dataTask(with: request) { data, _, error in
                
                guard let data = data, error == nil else {
                    
                    completion(.failure(APIError.failedToGetData))
                    return
                }
                
                do {
                    
                    let result = try JSONDecoder().decode([Joke].self, from: data)
                    
                    DispatchQueue.main.async {
                        
                        completion(.success(result))
                    }
                }catch{
                    
                    completion(.failure(error))
                }
            }
            
            task.resume()
        }
    }
    
    
    // MARK: - Functions
    
    /// The function is to create a url request
    private func createRequest(
        with url: URL?,
        type: HTTPMethod,
        completion: @escaping (URLRequest) -> Void
    ) {
        
        guard let apiURL = url else { return }
        
        var request = URLRequest(url: apiURL)
        request.timeoutInterval = 30
        completion(request)
    }
}
