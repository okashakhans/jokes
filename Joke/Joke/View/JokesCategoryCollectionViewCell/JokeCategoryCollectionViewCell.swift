//
//  JokeCategoryCollectionViewCell.swift
//  Joke
//
//  Created by Okasha Khan on 15/06/2021.
//

import UIKit

class JokeCategoryCollectionViewCell: UICollectionViewCell {

    static let identifier = "JokeCategoryCollectionViewCell"
    
    @IBOutlet weak var imageOuterView: UIView!
    @IBOutlet weak var categorySubheading: UILabel!
    @IBOutlet weak var categoryTitle: UILabel!
    @IBOutlet weak var categoryImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
         configureDesign()
    }
    
    private func configureDesign(){
    
        self.imageOuterView.layer.cornerRadius = self.categoryImage.height / 10
    }
    
    func configure(with image:String, title:String, totalJokes:Int, tintColor:UIColor) {
        
        self.categoryImage.tintColor = tintColor
        self.categoryImage.image = UIImage(systemName: image)
        self.categoryTitle.text = title
        self.categorySubheading.text = " \(totalJokes) jokes available"
    }

}
